/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.spring;

import com.eir.ecom.db.dao.ConfigurationDao;
import com.eir.ecom.db.dao.Dao;
import com.eir.ecom.db.dao.ResponseMessageDao;
import com.eir.ecom.db.dao.ServiceDocDao;
import com.eir.ecom.db.dao.SessionsDao;
import com.eir.ecom.db.dao.UserAccountDao;
import java.util.Calendar;
import javax.naming.ConfigurationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class SInit {

    public static Dao dao;

    public static UserAccountDao userAccountDao;
    public static SessionsDao sessionsDao;
    public static ResponseMessageDao responseMessageDao;
    public static ConfigurationDao configurationDao;
    public static ServiceDocDao serviceDocDao;

    public static UserAccountDao getUserAccountDao() {
        return userAccountDao;
    }

    public static void setUserAccountDao(UserAccountDao userAccountDao) {
        SInit.userAccountDao = userAccountDao;
    }

    public static SessionsDao getSessionsDao() {
        return sessionsDao;
    }

    public static void setSessionsDao(SessionsDao sessionsDao) {
        SInit.sessionsDao = sessionsDao;
    }

    public static ResponseMessageDao getResponseMessageDao() {
        return responseMessageDao;
    }

    public static void setResponseMessageDao(ResponseMessageDao responseMessageDao) {
        SInit.responseMessageDao = responseMessageDao;
    }

    public static ConfigurationDao getConfigurationDao() {
        return configurationDao;
    }

    public static void setConfigurationDao(ConfigurationDao configurationDao) {
        SInit.configurationDao = configurationDao;
    }

    public static ServiceDocDao getServiceDocDao() {
        return serviceDocDao;
    }

    public static void setServiceDocDao(ServiceDocDao serviceDocDao) {
        SInit.serviceDocDao = serviceDocDao;
    }
    
    

    public void InitService(String contextLocation) throws ConfigurationException {
        try {
            System.out.println("Starting DB Init");

            Calendar timeStart = Calendar.getInstance();

            ApplicationContext context = new FileSystemXmlApplicationContext(contextLocation);
            setUserAccountDao(context.getBean("UserAccountDao", UserAccountDao.class));
            setSessionsDao(context.getBean("SessionsDao", SessionsDao.class));
            setResponseMessageDao(context.getBean("ResponseMessageDao", ResponseMessageDao.class));
            setConfigurationDao(context.getBean("ConfigurationDao", ConfigurationDao.class));
            setServiceDocDao(context.getBean("ServiceDocDao", ServiceDocDao.class));

            Calendar timeEnd = Calendar.getInstance();
            long diff = timeEnd.getTimeInMillis() - timeStart.getTimeInMillis();
            double seconds = (double) diff / 1000;
            long minutes = (long) seconds / 60000;

            System.out.println("DB Started");
            System.out.println("Init Performance: " + minutes + ":" + seconds + " seconds");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
