/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.main;

import com.eir.ecom.db.spring.SInit;
import java.io.File;
import java.util.Scanner;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Andri D. Septian
 */
@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration(exclude = HibernateJpaAutoConfiguration.class)
//@EnableAutoConfiguration(exclude = {HibernateJpaAutoConfiguration.class, WebMvcAutoConfiguration.class})
public class DatabaseTesting {

    public static void main(String[] args) {
        SpringApplication.run(DatabaseTesting.class, args);
        try {

            String data = "";
            File myObj = new File("config.json");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                data += myReader.nextLine();
//                System.out.println(data);
            }
            myReader.close();

            JSONObject configfile = new JSONObject(data);

            SInit spring = new SInit();
            spring.InitService(configfile.getString("context_url"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @RequestMapping(value = "/userRegistration", method = RequestMethod.POST)
//    public String userRegistration(@RequestBody(required = false) String body,
//            HttpServletRequest request) throws ParseException {
//        
////        AdditionalData.JSON te = new JSONObject();
//        
//        return null;
//
//    }
//
//    @RequestMapping(value = "/input", method = RequestMethod.GET)
//    public String inputCategory(@RequestBody(required = false) String body,
//            @RequestParam(value = "categoryName") String categoryName,
//            @RequestParam(value = "description") String description,
//            HttpServletRequest request) throws ParseException {
//
//        System.out.println(categoryName);
//        System.out.println(description);
//
//        Category category = new Category();
//        category.setCategoryName(categoryName);
//        category.setDescription(description);
//        category = SInit.getCategoryDao().saveorupdate(category);
//
//        return "masooook";
//
//    }
//
//    @RequestMapping(value = "/monthperform", method = RequestMethod.POST)
//    public String getmothperfromance(@RequestBody(required = false) String body,
//            HttpServletRequest request) throws ParseException {
//
//        JSONObject jsonBody = new JSONObject(body);
//
////        String startDate = jsonBody.getString("start");
////        String endDate = jsonBody.getString("end");
//        JSONObject response = new JSONObject();
//        JSONArray dataArray = new JSONArray();
//
//        try {
//
//            for (int i = 1; i <= 12; i++) {
//
//                String QueryString = "SELECT * FROM user_account a WHERE a.reg_time BETWEEN '2020-[month]-01' AND '2020-[month]-01'";
//
//                String monthSeq = StringUtils.leftPad(String.valueOf(i), 2, "0");
//                QueryString = QueryString.replace("[month]", monthSeq);
//
//                System.out.println("query: " + QueryString);
//                List<UserAccount> uaList = SInit.getUserAccountDao().Query.Native.list(QueryString);
//
//                System.out.println(monthSeq + " perform: " + uaList.size());
//                dataArray.put(monthSeq + " perform: " + uaList.size());
//
//            }
//
//            response.put("data", dataArray);
//
//            return response.toString();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    @RequestMapping(value = "/get", method = RequestMethod.GET)
//    public String getAllData(HttpServletRequest request) throws ParseException {
//        try {
//            String QueryString = "SELECT * FROM user_account a";
//
//            List<UserAccount> uaList = SInit.getUserAccountDao().Query.Native.list(QueryString);
//
//            String html = "<!DOCTYPE html><html>[head][body]</html>";
//
//            String head = "<head>\n"
//                    + "<style>\n"
//                    + "table {\n"
//                    + "  font-family: arial, sans-serif;\n"
//                    + "  border-collapse: collapse;\n"
//                    + "  width: 100%;\n"
//                    + "}\n"
//                    + "\n"
//                    + "td, th {\n"
//                    + "  border: 1px solid #dddddd;\n"
//                    + "  text-align: left;\n"
//                    + "  padding: 8px;\n"
//                    + "}\n"
//                    + "\n"
//                    + "tr:nth-child(even) {\n"
//                    + "  background-color: #dddddd;\n"
//                    + "}\n"
//                    + "</style>\n"
//                    + "</head>";
//
//            String body = "<body>[bodyvalue]</body>";
//
//            String result = "<table>   "
//                    + "  <tr>\n"
//                    + "    <th>Registration Date</th>\n"
//                    + "    <th>Full Name</th>\n"
//                    + "    <th>Username</th>\n"
//                    + "    <th>Password</th>\n"
//                    + "    <th>Phone Number</th>\n"
//                    + "    <th>Email</th>\n"
//                    + "    <th>Avatar</th>\n"
//                    + "  </tr>";
//
//            for (UserAccount userAccount : uaList) {
//                String row = "<tr>\n"
//                        + "    <td>[regdate]</td>\n"
//                        + "    <td>[fullname]</td>\n"
//                        + "    <td>[username]</td>\n"
//                        + "    <td>[password]</td>\n"
//                        + "    <td>[phonenumber]</td>\n"
//                        + "    <td>[email]</td>\n"
//                        + "    <td><img src=\"[avatar]\" alt=\"[username] avatar\"></td>\n"
//                        + "  </tr>";
//
//                row = row.replace("[regdate]", userAccount.getReqTime().toString())
//                        .replace("[fullname]", userAccount.getFullName())
//                        .replace("[username]", userAccount.getUsername())
//                        .replace("[password]", userAccount.getPassword())
//                        .replace("[phonenumber]", userAccount.getPhoneNumber())
//                        .replace("[email]", userAccount.getEmail())
//                        .replace("[avatar]", userAccount.getAvatar());
//
//                result += row;
//            }
//
//            result += "</table>";
//
//            body = body.replace("[bodyvalue]", result);
//
//            html = html.replace("[head]", head)
//                    .replace("[body]", body);
//
//            return html;
//
//        } catch (Exception e) {
//            e.printStackTrace();
//            return "gagal";
//        }
//    }
}
