/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Andri D. Septian
 */
public class syntaxtesting {

    public static void main(String[] args) throws ParseException {

        String[] emaildom = {"gmail", "yahoo", "outlook", "yandex", "mail", "icloud", "apple"};

        for (int i = 0; i < 10; i++) {

            String firstname = generatingRandomName();
            String lastname = generatingRandomName();

            String fullname = firstname + " " + lastname;
            String username = firstname.toLowerCase() + getRandomDoubleBetweenRange(1, 999);
            String password = RandomStringUtils.random(getRandomDoubleBetweenRange(6, 10), true, true).toLowerCase();
            String phonenumber = "+62" + getRandomDoubleBetweenRange(11, 99) + getRandomDoubleBetweenRange(1000, 9999) + getRandomDoubleBetweenRange(1000, 9999);
            String email = username + "@" + emaildom[getRandomDoubleBetweenRange(0, emaildom.length - 1)] + ".com";

            System.out.println(
                    fullname + " , "
                    + username + " , "
                    + password + " , "
                    + phonenumber + " , "
                    + email
            );

        }
    }

    public static int getRandomDoubleBetweenRange(int min, int max) {
        int x = (int) (Math.random() * ((max - min) + 1)) + min;
        return x;
    }

    public static String generatingRandom() throws ParseException {
        for (int i = 0; i < 100; i++) {
            String year = "2020";
            String month = String.valueOf(getRandomDoubleBetweenRange(1, 12));
            month = StringUtils.leftPad(month, 2, "0");
            String date = String.valueOf(getRandomDoubleBetweenRange(1, 30));
            date = StringUtils.leftPad(date, 2, "0");
            String convDate = year + "-" + month + "-" + date;

            String generatedString = RandomStringUtils.random(10, true, false);

            Date regDate = new SimpleDateFormat("yyyy-MM-dd").parse(convDate);

            System.out.println(regDate + " - " + generatedString);
        }

        return null;
    }

    public static String generatingRandomName() {
        char[] life = {'a', 'i', 'u', 'e', 'o'};

        int lengthName = getRandomDoubleBetweenRange(4, 8);

        String randName = "";
        for (int i = 0; i < lengthName; i++) {
//            int checkoddevent = getRandomDoubleBetweenRange(1, 10);
            if (i % 2 == 0) {
                randName += life[getRandomDoubleBetweenRange(0, life.length - 1)];
            } else {
                char ch = RandomStringUtils.random(1, true, false).toLowerCase().toCharArray()[0];
                while (true) {
                    if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'x' || ch == 'z' || ch == 'q') {
                        ch = RandomStringUtils.random(1, true, false).toLowerCase().toCharArray()[0];
                    } else {
                        randName += ch;
                        break;
                    }
                }
            }

            if (i == 0) {
                randName = randName.toUpperCase();
            }
        }

        return randName;
    }

    public static String generatingPassword(String firstname, String lastname) {

        String password = "";

        switch (getRandomDoubleBetweenRange(1, 5)) {
            case 1:
                // code block
                password = generatingRandomName() + getRandomDoubleBetweenRange(1, 999);
                break;
            case 2:
                // code block
                password = firstname.toLowerCase() + getRandomDoubleBetweenRange(1, 999);
                break;
            case 3:
                // code block
                password = lastname.toLowerCase() + getRandomDoubleBetweenRange(1, 999);
                break;
            case 4:
                // code block
                password = firstname.toLowerCase() + lastname.toLowerCase() + getRandomDoubleBetweenRange(1, 99);
                break;
            default:
                // code block
                password = RandomStringUtils.random(getRandomDoubleBetweenRange(6, 10), true, true).toLowerCase();

        }

        return password;
    }

    //    public void UserAccountAutoGenerate() throws ParseException {
//        String[] emaildom = {"gmail", "yahoo", "outlook", "yandex", "mail", "icloud", "apple"};
//        for (int i = 0; i < 200; i++) {
//
//            String firstname = generatingRandomName();
//            String lastname = generatingRandomName();
//
//            String fullname = firstname + " " + lastname;
//            String username = firstname.toLowerCase() + getRandomDoubleBetweenRange(1, 999);
//            String password = generatingPassword(firstname, lastname);
//            String phonenumber = "+628" + getRandomDoubleBetweenRange(11, 99) + getRandomDoubleBetweenRange(1000, 9999) + getRandomDoubleBetweenRange(1000, 9999);
//            String email = username + "@" + emaildom[getRandomDoubleBetweenRange(0, emaildom.length - 1)] + ".com";
//
//            String year = "2020";
//            String month = String.valueOf(getRandomDoubleBetweenRange(1, 12));
//            month = StringUtils.leftPad(month, 2, "0");
//            String date = String.valueOf(getRandomDoubleBetweenRange(1, 30));
//            date = StringUtils.leftPad(date, 2, "0");
//            String convDate = year + "-" + month + "-" + date;
//
//            Date regDate = new SimpleDateFormat("yyyy-MM-dd").parse(convDate);
//
//            UserAccount ua = new UserAccount();
//            ua.setReqTime(regDate);
//            ua.setUsername(username);
//            ua.setPassword(password);
//            ua.setFullName(fullname);
//            ua.setPhoneNumber(phonenumber);
//            ua.setEmail(email);
//            ua.setStatus(1);
//            ua.setUserType(2);
//            ua.setAvatar("https://api.adorable.io/avatars/285/" + username + ".png");
//            SInit.getUserAccountDao().saveorupdate(ua);
//
//            System.out.println(""
//                    + StringUtils.rightPad(convDate, 20, " ")
//                    + StringUtils.rightPad(fullname, 20, " ")
//                    + StringUtils.rightPad(username, 20, " ")
//                    + StringUtils.rightPad(password, 20, " ")
//                    + StringUtils.rightPad(phonenumber, 20, " ")
//                    + StringUtils.rightPad(email, 20, " ")
//            );
//
//        }
//        System.out.println("Start generating");
//        for (int i = 0; i < 200; i++) {
//            String year = "2020";
//            String month = String.valueOf(getRandomDoubleBetweenRange(1, 12));
//            month = StringUtils.leftPad(month, 2, "0");
//            String date = String.valueOf(getRandomDoubleBetweenRange(1, 30));
//            date = StringUtils.leftPad(date, 2, "0");
//            String convDate = year + "-" + month + "-" + date;
//
//            String generatedString = "User_" + RandomStringUtils.random(10, true, false);
//
//            Date regDate = new SimpleDateFormat("yyyy-MM-dd").parse(convDate);
//
//            System.out.println(regDate + " - " + generatedString);
//
//            UserAccount ua = new UserAccount();
//            ua.setReqTime(regDate);
//            ua.setUsername(generatedString);
//            SInit.getUserAccountDao().saveorupdate(ua);
//        }
//        System.out.println("End Generating");
//    }
//
}
