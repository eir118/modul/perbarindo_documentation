package com.eir.ecom.db.enums;

import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Andri D. Septian
 */
public class AdditionalData {

    private JSONObject data;

    public AdditionalData(JSONObject data) {
        this.data = data;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String toString() {
        return data.toString();
    }
}
