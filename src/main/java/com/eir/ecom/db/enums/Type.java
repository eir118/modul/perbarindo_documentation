/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.enums;

/**
 *
 * @author Andri D. Septian
 */
public class Type {

    public enum User {
        DEVELOPER(0),
        SUPERADMIN(1),
        ADMIN(2),
        USER(3);

        private int type;

        private User(int type) {
            this.type = type;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String toString() {
            String value = "[name]([int])";
            return value.replace("[name]", name()).replace("[int]", String.valueOf(getType()));
        }
    }
}
