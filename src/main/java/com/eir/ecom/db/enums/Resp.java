/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.enums;

import com.eir.ecom.db.model.ResponseMessage;
import com.eir.ecom.db.spring.SInit;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
public class Resp {

    /**
     * Wiki - Response Message
     *
     * Response Message ID Format AABB
     *
     * AA = Response Initials 
     *
     * BB = Response Code
     *
     * Example: 1099 10 -> General Response Initials
     *
     * 99 -> Failed Response Code
     */

    private static String public_code;
    private static String public_name;

    /* 10XX */
    public enum GENERAL {
        SUCCESS(1000),
        NOT_FOUND(1095),
        MISSING_PARAMETER(1096),
        TIMEOUT(1097),
        INTERNAL_ERROR(1098),
        FAILED(1099);

        private long code;

        private GENERAL(long code) {
            this.code = code;
            public_code = String.valueOf(code);
            public_name = name();
        }

    }

    public enum USER {
        /*USER*/
        LOGIN_SUCCESS(1100),
        REGISTRATION_SUCCESS(1101),
        WRONG_PASSWORD(1111),
        NOT_FOUND(1112),
        BLOCKED(1113),
        NAME_ALREADY_USED(1114),
        PHONE_ALREADY_USED(1115),
        NIK_ALREADY_USED(1116),
        EMAIL_ALREADY_USED(1117),
        WRONG_PARAMETER(1198),
        INTERNAL_LOGIN_ERROR(1199);

        private long code;

        private Read get;

        public String getRC() {
            return get.rc();
        }

        public String getRM() {
            return get.rm();
        }

        public JSONObject getJson() {
            return get.json();
        }

        private USER(long code) {
            this.code = code;
            public_code = String.valueOf(code);
            public_name = name();
        }

    }

    private static class Read {

        public String rc() {
            ResponseMessage resp = SInit.getResponseMessageDao().findById(public_code);
            if (resp != null) {
                return resp.getRc();
            } else {
                return public_code;
            }
        }

        public String rm() {
            ResponseMessage resp = SInit.getResponseMessageDao().findById(public_code);
            if (resp != null) {
                return resp.getRm() + ". [" + public_code + "]";
            } else {
                save(resp);
                return public_name + ". [" + public_code + "]";
            }
        }

        public JSONObject json() {

            ResponseMessage resp = SInit.getResponseMessageDao().findById(public_code);
            if (resp != null) {

                JSONObject jResp = new JSONObject();
                jResp.put("rc", resp.getRc());
                jResp.put("rm", resp.getRm() + ". [" + public_code + "]");

                return jResp;
            } else {

                save(resp);

                JSONObject jResp = new JSONObject();
                jResp.put("rc", "NF");
                jResp.put("rm", public_name + ". [" + public_code + "]");

                return jResp;
            }
        }

        private static void save(ResponseMessage resp) {
            resp = new ResponseMessage();
            resp.setId(public_code);
            resp.setResponseName(public_name);
            resp.setRm(public_name.toLowerCase());
            resp.setRc(public_code.substring(2));
            SInit.getResponseMessageDao().saveorupdate(resp);
        }
    }

}
