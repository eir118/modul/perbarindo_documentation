/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.enums;

/**
 *
 * @author Andri D. Septian
 */
public enum Status {

    INACTIVE(0),
    NOT_VERIFIED(1),
    ACTIVE(2),
    SUSPENDED(3),
    BLOCKED(4),
    DELETED(5);

    private int status;

    private Status(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String toString() {
        String value = "[name]([int])";
        return value.replace("[name]", name()).replace("[int]", String.valueOf(getStatus()));
    }

    public enum User {
        INACTIVE(0),
        NOT_VERIFIED(1),
        ACTIVE(2),
        RESET_PASSWORD(3),
        EXPIRED(4),
        SUSPENDED(5),
        BLOCKED(6),
        DELETED(7);

        private int status;

        private User(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int user) {
            this.status = user;
        }

        public String toString() {
            String value = "[name]([int])";
            return value.replace("[name]", name()).replace("[int]", String.valueOf(getStatus()));
        }
    }

    public enum Data {
        DRAFT(0), PUBLISH(1), DELETED(2);

        private int status;

        private Data(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }

        public void setData(int status) {
            this.status = status;
        }

        public String toString() {
            String value = "[name]([int])";
            return value.replace("[name]", name()).replace("[int]", String.valueOf(getStatus()));
        }
    }
}
