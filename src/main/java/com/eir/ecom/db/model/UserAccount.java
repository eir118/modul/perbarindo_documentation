/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import com.eir.ecom.db.enums.Status;
import com.eir.ecom.db.enums.Type;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "user_account")
public class UserAccount implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "user_id_seq")
    @SequenceGenerator(name = "user_id_seq", sequenceName = "user_id_seq")
    @Column(name = "user_id", nullable = false)
    private Long user_id;

    @Column(name = "reg_time", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date reqTime = new Date();

    @Column(name = "mod_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modTime;

    @Column(name = "status", nullable = false)
    private Status.User status = Status.User.NOT_VERIFIED;

    @Column(name = "fail_login", nullable = false)
    private Integer failLogin = 0;

    @Column(name = "user_type", nullable = false)
    private Type.User userType = Type.User.USER;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "full_name", nullable = false)
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address", length = 999)
    private String address;

    @Column(name = "avatar", length = 999)
    private String avatar;

    @Column(name = "additionalData")
    @org.hibernate.annotations.Type(type = "text")
    private String additionalData;

    @PreUpdate
    protected void onUpdate() {
        setModTime(new Date());
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Date getReqTime() {
        return reqTime;
    }

    public void setReqTime(Date reqTime) {
        this.reqTime = reqTime;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public Status.User getStatus() {
        return status;
    }

    public void setStatus(Status.User status) {
        this.status = status;
    }

    public Integer getFailLogin() {
        return failLogin;
    }

    public void setFailLogin(Integer failLogin) {
        this.failLogin = failLogin;
    }

    public Type.User getUserType() {
        return userType;
    }

    public void setUserType(Type.User userType) {
        this.userType = userType;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }
}
