/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "configuration")
public class Configuration implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "config_id_seq")
    @SequenceGenerator(name = "config_id_seq", sequenceName = "config_id_seq")
    @Column(name = "config_id", nullable = false)
    private Long configId;

    @Column(name = "config_name", nullable = false, unique = true)
    private String configName;
    
    @Column(name = "value", nullable = false)
    private String value;
    
    @Column(name = "description")
    @Type(type = "text")
    private String description;
    
    @Column(name = "additional_data")
    @Type(type = "text")
    private String additionalData;

    public Long getConfigId() {
        return configId;
    }

    public void setConfigId(Long configId) {
        this.configId = configId;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }
    
}
