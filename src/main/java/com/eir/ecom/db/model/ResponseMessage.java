/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "response_message")
public class ResponseMessage implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    private String id;

    @Column(name = "response_name", nullable = false)
    private String responseName;

    @Column(name = "rc", nullable = false)
    private String rc;

    @Column(name = "rm", nullable = false)
    private String rm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

}
