/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import com.eir.ecom.db.enums.AdditionalData;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "sessions")
public class Sessions implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "ses_id_seq")
    @SequenceGenerator(name = "ses_id_seq", sequenceName = "ses_id_seq")
    @Column(name = "session_id", nullable = false)
    private Long sessionId;

    @Column(name = "req_time", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date reqTime;

    @Column(name = "res_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date resTime;

    @Column(name = "performance_time")
    private Long performanceTime;

    @Column(name = "path", length = 999)
    private String path;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "req_body")
    @Type(type = "text")
    private String reqBody;

    @Column(name = "res_body")
    @Type(type = "text")
    private String resBody;

    @Column(name = "rc")
    private String rc;

    @Column(name = "rm")
    private String rm;

    @Column(name = "http_code")
    private String httpCode;

    @Column(name = "additionalData")
    @Type(type = "text")
    private String additionalData;

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }


    @Column(name = "info")
    @Type(type = "text")
    private String info;

    @Column(name = "exception")
    @Type(type = "text")
    private String exception;

    @PrePersist
    protected void onCreate() {
        setReqTime(new Date());
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Date getReqTime() {
        return reqTime;
    }

    public void setReqTime(Date reqTime) {
        this.reqTime = reqTime;
    }

    public Date getResTime() {
        return resTime;
    }

    public void setResTime(Date resTime) {
        this.resTime = resTime;
    }

    public Long getPerformanceTime() {
        return performanceTime;
    }

    public void setPerformanceTime(Long performanceTime) {
        this.performanceTime = performanceTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getReqBody() {
        return reqBody;
    }

    public void setReqBody(String reqBody) {
        this.reqBody = reqBody;
    }

    public String getResBody() {
        return resBody;
    }

    public void setResBody(String resBody) {
        this.resBody = resBody;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getRm() {
        return rm;
    }

    public void setRm(String rm) {
        this.rm = rm;
    }

    public String getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(String httpCode) {
        this.httpCode = httpCode;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

}
