/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.UserAccount;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "UserAccountDao")
@Transactional
public class UserAccountDao extends Dao {
    
    public EntityManager getEm() {
        return em;
    }

    Query Query = new Query();

    public UserAccount saveorupdate(UserAccount UserApp) {
        try {
            if (UserApp.getUser_id() == null) {
                em.persist(UserApp);
            } else {
                em.merge(UserApp);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return UserApp;
    }

    public UserAccount findByUsername(String username) {
        try {
            return (UserAccount) em.createQuery("SELECT a FROM UserAccount a WHERE a.username = :username")
                    .setParameter("username", username)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public UserAccount findByEmail(String email) {
        try {
            return (UserAccount) em.createQuery("SELECT a FROM User a WHERE a.email = :email")
                    .setParameter("email", email)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public class Query {

        Native Native = new Native();

        public UserAccount single(String query) {
            try {
                return (UserAccount) em.createQuery(query, UserAccount.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public List<UserAccount> list(String query) {
            try {
                return (List<UserAccount>) em.createQuery(query, UserAccount.class)
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public class Native {

            public UserAccount single(String query) {
                try {

                    return (UserAccount) em.createNativeQuery(query, UserAccount.class)
                            .setMaxResults(1)
                            .getSingleResult();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public List<UserAccount> list(String query) {
                try {
                    return (List<UserAccount>) em.createNativeQuery(query, UserAccount.class)
                            .getResultList();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}
