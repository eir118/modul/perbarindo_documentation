/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.ResponseMessage;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "ResponseMessageDao")
@Transactional
public class ResponseMessageDao extends Dao {
    
    public EntityManager getEm() {
        return em;
    }

    Query Query = new Query();

    public ResponseMessage saveorupdate(ResponseMessage res) {
        try {
            if (res.getId() == null) {
                em.persist(res);
            } else {
                em.merge(res);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return res;
    }

    public ResponseMessage findById(String id) {
        try {
            return (ResponseMessage) em.createQuery("SELECT a FROM ResponseMessage a WHERE a.responseId = :responseId")
                    .setParameter("responseId", id)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public ResponseMessage findByResponseName(String responseName) {
        try {
            return (ResponseMessage) em.createQuery("SELECT a FROM ResponseMessage a WHERE a.responseName = :responseName")
                    .setParameter("responseName", responseName)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            nre.printStackTrace();
            return null;
        }
    }

    public class Query {

        Native Native = new Native();

        public ResponseMessage single(String query) {
            try {
                return (ResponseMessage) em.createQuery(query, ResponseMessage.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public List<ResponseMessage> list(String query) {
            try {
                return (List<ResponseMessage>) em.createQuery(query, ResponseMessage.class)
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public class Native {

            public ResponseMessage single(String query) {
                try {

                    return (ResponseMessage) em.createNativeQuery(query, ResponseMessage.class)
                            .setMaxResults(1)
                            .getSingleResult();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public List<ResponseMessage> list(String query) {
                try {
                    return (List<ResponseMessage>) em.createNativeQuery(query, ResponseMessage.class)
                            .getResultList();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

}
