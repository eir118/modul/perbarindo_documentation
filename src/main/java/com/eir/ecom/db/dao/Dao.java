/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;

/**
 *
 * @author Andri D. Septian
 */
public class Dao {

    @Resource(name = "sharedEntityManagerExternal")
    public EntityManager em;

    public Object objQuery(javax.persistence.Query que) {
        try {

            List<Object> objList = que.getResultList();

            if (objList != null) {
                if (objList.size() >= 1) {
                    return objList;
                } else {
                    return objList.get(0);
                }
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
