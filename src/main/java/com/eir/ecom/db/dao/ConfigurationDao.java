/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.Configuration;
import java.util.List;
import javax.persistence.EntityManager;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "ConfigurationDao")
@Transactional
public class ConfigurationDao extends Dao {

    public EntityManager getEm() {
        return em;
    }

    public Configuration saveorupdate(Configuration dat) {
        try {
            if (dat.getConfigId() == null) {
                em.persist(dat);
            } else {
                em.merge(dat);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return dat;
    }

    public Configuration findById(Long configId) {
        try {
            return (Configuration) em.createQuery("SELECT a FROM Configuration a WHERE a.configId = :configid ")
                    .setParameter("configid", configId)
                    .getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getValueByName(String configName) {
        try {
            return (String) em.createQuery("SELECT a FROM Configuration a WHERE a.configName = :configname", Configuration.class)
                    .setParameter("configname", configName)
                    .getSingleResult()
                    .getValue();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Object getValueObjectByName(String configName) {
        try {
            return (Object) em.createQuery("SELECT a FROM Configuration a WHERE a.configName = :configname", Configuration.class)
                    .setParameter("configname", configName)
                    .getSingleResult()
                    .getValue();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONObject getAdditionalDataByName(String configName) {
        try {
            return (JSONObject) em.createQuery("SELECT a FROM Configuration a WHERE a.configName = :configname", Configuration.class)
                    .setParameter("configname", configName)
                    .getSingleResult()
                    .getAdditionalData();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
