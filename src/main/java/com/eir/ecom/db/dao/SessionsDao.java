/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.Sessions;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "SessionsDao")
@Transactional
public class SessionsDao extends Dao {

    public EntityManager getEm() {
        return em;
    }
    
    public Sessions saveorupdate(Sessions id) {
        try {
            if (id.getSessionId() == null) {
                em.persist(id);
            } else {
                em.merge(id);
            }
        } catch (Exception ex) {
            return null;
        }
        return id;
    }

}
